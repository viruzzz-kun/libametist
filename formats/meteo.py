# !/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright 2014 Mikhael Malkov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from cStringIO import StringIO
from .. import Message, encode_default, MessageFormatRegistry
import re

__author__ = 'viruzzz-kun'
__created__ = '15.06.2014'

re_pattern_wmo = re.compile(
    r"(\r\r\n|\r\n|\r|\n|\n\r)(?P<num_wmo>\d{3})"
    r"(\r\r\n|\r\n|\r|\n|\n\r)(?P<ahl1>.{6})\s?(?P<ahl2>.{4})\s(?P<timestamp>\d{6})(\s(?P<group>.{3}))?"
    r"(\r\r\n|\r\n|\r|\n|\n\r)(?P<body>(?P<type>(BUFR)|(Talarna)|(FAX)|(GRIB))?.*)",
    flags=re.DOTALL)

re_pattern_hms = re.compile(
    r"(?P<hms_addr>\d{3} [\d/]{4,6})=?\x0e?([Нn](?P<num_hms>\d{3})?)?.*?"
    r"(\r\r\n|\r\n|\r|\n|\n\r)\x0e?(?P<ahl1>.{6})\s?(?P<ahl2>.{4})\s(?P<timestamp>\d{6})(\s(?P<group>.{3}))?"
    r"(\r\r\n|\r\n|\r|\n|\n\r)(?P<body>.*)",
    flags=re.DOTALL)


def encode_wmo(message, encoding='utf-8', delimiter='\r\r\n'):
    """
    @rtype: bytes
    @return: flat representation on message
    """
    # header is unicode which is then converted to desired encoding
    header = StringIO()
    header.write(u'%s%03d%s' % (delimiter, message.get('num', 0), delimiter))
    header.write(u'%s %s %s' % (message['ahl'][:6], message['ahl'][6:], message['timestamp']))
    if message.get('group'):
        header.write(u' %s' % message['group'])
    header.write(delimiter)
    # Trying converting payload to flat format
    if isinstance(message.payload, unicode):
        payload = delimiter.join(message.payload.splitlines()) + delimiter
    elif isinstance(message.payload, (bytes, Message)):
        payload = message.payload
    else:
        raise Exception('Unsupported payload type')
    # encoded message is bytes
    body = encode_default(payload, encoding, delimiter)
    output = StringIO()
    output.write(header.getvalue().encode(encoding))
    output.write(body)
    return output.getvalue()


def decode_wmo(string, encoding='utf-8'):
    """Decodes WMO-encoded message

    @param string: a packet to be decoded
    @type string: str
    @rtype: Message
    @return decoded message
    """
    message = Message()
    matchObject = re_pattern_wmo.match(string)
    if not matchObject:
        raise ValueError(u"'raw' is not WMO-encoded message")
    tokens = matchObject.groupdict()
    if tokens['type'] in ("BUFR", "FAX", "GRIB"):
        message.tag('binary')
        message.tag(tokens['type'])
        message.payload = tokens['body']
    else:
        message.tag('text')
        message.payload = tokens['body'].decode(encoding) if encoding else tokens['body']
    message['timestamp'] = tokens["timestamp"]
    message['ahl'] = tokens["ahl1"] + tokens["ahl2"]
    message['group'] = tokens['group']
    if encoding:
        message['ahl'] = message['ahl'].decode(encoding)
        message['group'] = message['group'].decode(encoding) if message.get('group') else None
        message['encoding'] = encoding
    message.format = 'wmo'
    return message


def encode_hms(message, encoding='utf-8', delimiter='\r\r\n'):
    """

    :type message: Message
    :type encoding: str | unicode
    :type delimiter: str | unicode

    :param message:
    :param encoding:
    :param delimiter:
    :return:
    """
    header = [
        u'%s=' % (message.get('hms_addr') or u'000 00000/'),
        u'\x0eН%03d\002' % message.get('num', 0),
        delimiter,
        u'\x0e%s %s %s' % (message['ahl'][:6], message['ahl'][6:], message['timestamp']),
        u' %s' % message['group'] if message.get('group') else u'',
        delimiter,
    ]

    # Trying converting payload to flat format
    if isinstance(message.payload, unicode):
        payload = delimiter.join(message.payload.splitlines()) + delimiter
    elif isinstance(message.payload, (bytes, Message)):
        payload = message.payload
    else:
        raise Exception('Unsupported payload type')

    body = encode_default(payload, encoding, delimiter)

    output = StringIO()
    output.write(u''.join(header).encode(encoding))
    output.write(body)

    return output.getvalue()


def decode_hms(string, encoding=None):
    matchObject = re_pattern_hms.match(string)
    if not matchObject:
        raise ValueError(u"'string' is not HMS-encoded message")
    tokens = matchObject.groupdict()
    message = Message()
    message['hms_addr'] = tokens['hms_addr']
    message.payload = tokens['body'].decode(encoding) if encoding else tokens['body']
    message.tag('hms', 'text')
    message['timestamp'] = tokens["timestamp"]
    message['ahl'] = tokens["ahl1"] + tokens["ahl2"]
    message['group'] = tokens['group']
    if encoding:
        message['ahl'] = message['ahl'].decode(encoding)
        message['group'] = message['group'].decode(encoding) if message.get('group') else None
        message['encoding'] = encoding
    message.format = 'hms'
    return message

MessageFormatRegistry.register_format('wmo', encode_wmo, decode_wmo)
MessageFormatRegistry.register_format('hms', encode_hms, decode_hms)