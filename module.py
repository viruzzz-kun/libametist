# !/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright 2014 Mikhael Malkov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import os
import sys

logging.basicConfig(
    format='%(asctime)s: %(process)5d [%(levelname)s] %(filename)s:%(lineno)d %(message)s',
    stream=sys.stderr,
    level=logging.DEBUG
)
from twisted.internet import reactor
from twisted.internet.protocol import connectionDone
from twisted.internet.stdio import StandardIO
from twisted.internet.error import ReactorNotRunning

from .stream_protocol import MsgPackStreamProtocol as StreamProtocol
# noinspection PyUnresolvedReferences
import libametist.formats

__author__ = 'viruzzz-kun'
__created__ = '20.04.2013'

class ModuleException(BaseException):
    pass


class TwistedModule(StreamProtocol):
    """Base class for channel modules. Provides basic functionality for communication with the Core"""

    def command_message(self, obj):
        self.router_messageReceived(obj['message'])

    def command_config(self, obj):
        self.configure(obj['config'])

    def command_stop(self, obj):
        self.stop()

    def command_msg_ack(self, obj):
        self.router_ackReceived(obj['magic'])

    def connectionLost(self, reason=connectionDone):
        self.stop()

    def sendMessage(self, message, magic=None):
        msg = {'command': 'message',
               'message': message,
               'magic': magic or os.urandom(16),
        }
        self.sendObject(msg)

    def sendAck(self, message_id):
        msg = {'command': 'msg_ack',
               'message_id': message_id,
        }
        self.sendObject(msg)

    def sendNAck(self, message_id):
        msg = {'command': 'msg_nack',
               'message_id': message_id,
        }
        self.sendObject(msg)

    def sendExtState(self, state):
        msg = {'command': 'extState',
               'extState': state,
        }
        self.sendObject(msg)

    def __init__(self):
        StreamProtocol.__init__(self)
        self.config = None

    def configure(self, config):
        """[override] Configure the Channel Module

        @param config: dict object with the configuration"""
        self.config = config
        logging.info('Got configuration. My name is "%s" now', self.config['name'])

    def run(self):
        StandardIO(self)
        reactor.run()

    @classmethod
    def main(cls):
        module = cls()
        module.run()

    def router_messageReceived(self, message):
        """[override, callback] Dispatch message received from Core

        @param message: api.Message"""

    def router_ackReceived(self, magic):
        """[override, callback] When core got and saved message"""

    def channel_messageReceived(self, message, magic=None):
        """[override, callback] Send message from channel to Core

        @param message: api.Message"""
        self.sendMessage(message, magic)

    def channel_ackReceived(self, message_id):
        """[override, callback] Acknowledge Core the message is processed

        @param message_id: message unique id"""
        self.sendAck(message_id)

    def channel_nackReceived(self, message_id):
        """[override, callback] Acknowledge Core the message is not processed

        @param message_id: message unique id"""
        self.sendAck(message_id)

    def stop(self):
        """Stop the current process"""
        try:
            reactor.callLater(2, reactor.stop)
        except ReactorNotRunning:
            pass
