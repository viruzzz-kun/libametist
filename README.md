AMETIST client library
======================

The library provides is used by both AMETIST server and plugins (either packaged with AMETIST or third-party)

Writing your own channel plugin
-------------------------------

To implement channel plugin you need to create runnable script (placed in `plugins` directory under your AMETIST 
installation), create a class derived from `libametist.module.TwistedModule`, override some methods and run `main()`
classmethod of derived class. For example:

    #!/usr/bin/env python

    from libametist.module import TwistedModule

    class Application(TwistedModule):
        def router_messageReceived(self, message):
            if message.get('filename'):
                with open('./.tmp/%s.dmp' % (message['filename']), 'wb') as fOut:
                    fOut.write(message.payload)
            self.sendAck(message.uid)

    if __name__ == "__main__":
        Application.main()

Methods meant to be overridden:

* `configure(self, config)` - is called only once when the channel is started and ready to receive its configuration.
    By default this initializes `config` property of the instance and must be overridden if custom behavior is desired.
    No measures should be taken by channel process before this method is run!
* `router_messageReceived(self, message)` - is called each time the Core wants to send a message via this channel
* `channel_messageReceived(self, message, magic=None)` - must be called each time a channel process received a message 
    and wants to send it to the Core. Usually, it is not overridden. `magic` is some random string used to distinguish 
    one message from another (Message.uid is set only after it is put to IOJ). If `magic` is None, the Core won't
    acknowledge channel process on success and `router_ackReceived` won't be executed.
* `router_ackReceived(self, magic)` - is called each time the Core wants to acknowledge the channel that it has 
    received a message (sent by `sendMessage` method) and successfully put it to the IOJ (IO Journal). `magic` is the 
    magic string received by Core along with the message.
* `channel_ackReceived(self, message_id)` - must be called each time a channel received an acknowledgement message
    (if supported) from the remote side
* `channel_nackReceived(self, message_id)` - must be called each time a channel received a negative acknowledgement
    (if supported) from the remote side

Writing your own message format
-------------------------------

To implement custom message format you need to create module (placed in `formats` subdirectory of `libametist`), 
implement encode function, decode function and run `libametist.MessageFormatRegistry.register_format` classmethod.

Encode function should have the following signature: `encode(message, encoding='utf-8', delimiter='\r\r\n')` where 
`message` is `libametist.Message` instance to be encoded, `encoding` is the destination encoding of the message and 
`delimiter` is sequence used to separate text lines used by some formats

Decode function should have signature `decode(data, encoding=None)`, where `data` is the binary data to be decoded
and `encoding` is the suggested encoding of any string data in message. If `encoding` is set any string data must be 
converted to unicode on decoding, and if encoding is `None`, no unicode decoding should be performed on string data.

Other
-----

Other documentation - later -___-