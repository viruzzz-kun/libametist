#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright 2014 Mikhael Malkov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import msgpack
from . import Message

__author__ = 'viruzzz-kun'
__created__ = '26.04.14'


def __encode_hook(obj):
    if isinstance(obj, set):
        return msgpack.ExtType(0, dump(sorted(obj)))
    elif isinstance(obj, Message):
        return msgpack.ExtType(1, dump(obj.__getstate__()))
    return obj


def __ext_hook(code, data):
    if code == 0:
        return set(load(data))
    elif code == 1:
        return Message(load(data))
    return msgpack.ExtType(code, data)


def load(chunk, **kwargs):
    return msgpack.unpackb(chunk, ext_hook=__ext_hook, encoding='utf-8', **kwargs)


def dump(o):
    return msgpack.packb(o, default=__encode_hook, use_bin_type=True)

