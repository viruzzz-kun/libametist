# !/usr/bin/python
# -*- coding: utf-8 -*-

#    Copyright 2014 Mikhael Malkov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import struct
from twisted.internet import protocol
from twisted.protocols.basic import LineOnlyReceiver
import cPickle as Pickle

from . import serialize


class BaseStreamProtocol(object):
    def objectReceived(self, obj):
        if not isinstance(obj, dict):
            return
        command = obj.get('command')
        if not command:
            return
        attr_name = 'command_%s' % command
        if hasattr(self, attr_name):
            getattr(self, attr_name)(obj)

    def prepareMessage(self, obj):
        raise NotImplemented

    def sendData(self, data):
        raise NotImplemented

    def sendObject(self, obj):
        self.sendData(self.prepareMessage(obj))

    def clearBuffer(self):
        pass


class PickleStreamProtocol(LineOnlyReceiver, BaseStreamProtocol):
    MAX_LENGTH = 16777216
    delimiter = '\n'

    def __init__(self):
        self.stringBuffer = []

    def lineReceived(self, line):
        if len(line) == 0:
            obj = Pickle.loads('\n'.join(self.stringBuffer))
            if obj:
                self.objectReceived(obj)
            self.stringBuffer = []
        else:
            self.stringBuffer.append(line)

    def prepareMessage(self, obj):
        return Pickle.dumps(obj) + '\n'

    def sendData(self, data):
        self.sendLine(data)

    def dataReceived(self, data):
        """
        Translates bytes into lines, and calls lineReceived.
        """
        lines = (self._buffer + data).split(self.delimiter)
        self._buffer = lines.pop(-1)
        for line in lines:
            if hasattr(self.transport, 'disconnecting') and self.transport.disconnecting:
                # this is necessary because the transport may be told to lose
                # the connection by a line within a larger packet, and it is
                # important to disregard all the lines in that packet following
                # the one that told it to close.
                return
            if len(line) > self.MAX_LENGTH:
                return self.lineLengthExceeded(line)
            else:
                self.lineReceived(line)
        if len(self._buffer) > self.MAX_LENGTH:
            return self.lineLengthExceeded(self._buffer)

    def clearBuffer(self):
        self.stringBuffer = []


class MsgPackStreamProtocol(protocol.Protocol, BaseStreamProtocol):
    header_struct = struct.Struct('!L')

    def __init__(self):
        self._buffer = b''
        self._wait_for_header = True
        self._expect_length = None

    def prepareMessage(self, obj):
        payload = serialize.dump(obj)
        header = self.header_struct.pack(len(payload))
        return header + payload

    def sendData(self, data):
        self.transport.write(data)

    def dataReceived(self, data):
        self._buffer += data

        while len(self._buffer) >= 4:
            expect_length, = self.header_struct.unpack(self._buffer[:4])
            end = 4 + expect_length
            if len(self._buffer) < end:
                return
            payload = self._buffer[4:end]
            self._buffer = self._buffer[end:]
            self.objectReceived(serialize.load(payload))

    def clearBuffer(self):
        self._buffer = b''
