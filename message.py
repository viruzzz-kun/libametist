# !/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright 2014 Mikhael Malkov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = 'viruzzz-kun'
__created__ = '08.06.2014'


class Message(object):
    class Header(object):
        def __init__(self, key, value=None):
            self.key = key
            self.value = value

        def set(self, value):
            self.value = value

    def __init__(self, source=None):
        """
        :param source: Source object to make a copy
        :type source: ContainerMessage | Message | None
        """
        self.headers = {}
        self['destinations'] = set()
        if isinstance(source, Message):
            self.headers = source.headers
            self._payload = source.payload
            self.uid = source.uid
            self.tags = source.tags
            self.format = source.format
        elif isinstance(source, dict):
            self.__setstate__(source)
        else:
            self._payload = None
            self.uid = None
            self.tags = set()
            self.format = 'raw'

    # dict emulation

    def __setitem__(self, key, value):
        if key.lower() in self.headers:
            self.headers[key.lower()].set(value)
        else:
            self.headers[key.lower()] = self.Header(key, value)

    def __getitem__(self, key):
        return self.headers[key.lower()].value

    def __delitem__(self, key):
        del self.headers[key.lower()]

    def __contains__(self, key):
        return key.lower() in self.headers

    def get(self, key, default=None):
        """Get header value
        :param key: header name (case insensitive)
        :type key: str|unicode
        :param default: default value if header was not found
        """
        if key.lower() in self.headers:
            return self.headers[key.lower()].value
        else:
            return default

    def update(self, source):
        """
        Update headers according to source
        :param source: Source of header data
        :type source: ContainerMessage | dict
        """
        for key, value in source.iteritems():
            self[key] = value

    def iteritems(self):
        for key, header in self.headers.iteritems():
            yield header.key, header.value

    def iterkeys(self):
        for key, header in self.headers.iteritems():
            yield header.key

    def itervalues(self):
        for key, header in self.headers.iteritems():
            yield header.value

    # (de)serialization

    def __getstate__(self):
        return {
            'headers': dict((header.key, header.value) for header in self.headers.itervalues()),
            'payload': self._payload,
            'tags': list(self.tags),
            'uid': self.uid,
            'format': self.format,
        }

    def __setstate__(self, source):
        self.headers = {}
        for key, val in source['headers'].iteritems():
            self.headers[key.lower()] = self.Header(key, val)
        self._payload = source['payload']
        self.tags = set(source['tags'])
        self.uid = source['uid']
        self.format = source.get('format')

    # data manipulation

    @property
    def encoding(self):
        return self.get('encoding')

    @property
    def payload(self):
        return self._payload

    @payload.setter
    def payload(self, value):
        self._payload = value

    def tag(self, *args):
        """
        Add tags to message
        :param args: tags
        :type args: list(basestring)
        """
        for arg in args:
            if isinstance(arg, basestring):
                self.tags.add(arg)

    def __ior__(self, other):
        """
        Add tags to message from set
        :param other: source set
        :type other: set
        """
        self.tags |= other
        return self

    def untag(self, *args):
        for arg in args:
            if arg in self.tags:
                self.tags.remove(arg)

    def has_tags(self, *tags):
        return all(tag in self.tags for tag in tags)

    def any_tags(self, *tags):
        return any(tag in self.tags for tag in tags)

    def no_tags(self, *tags):
        return not any(tag in self.tags for tag in tags)

    def as_string(self, encoding='utf-8', delimiter='\n'):
        return MessageFormatRegistry.encode(self.format, self, encoding, delimiter)

    @classmethod
    def from_string(cls, fmt, string, encoding=None):
        return MessageFormatRegistry.decode(fmt, string, encoding)

    def clone(self):
        return self.__class__(self)


class MessageFormatRegistry(object):
    __formats = {}

    @classmethod
    def register_format(cls, name, encoder, decoder):
        cls.__formats[name] = (encoder, decoder)

    @classmethod
    def decode(cls, fmt, data, encoding='utf-8'):
        decoder = cls.__formats[fmt][1]
        return decoder(data, encoding)

    @classmethod
    def encode(cls, fmt, msg, encoding='utf-8', delimiter='\n'):
        encoder = cls.__formats[fmt][0]
        return encoder(msg, encoding, delimiter)

    @classmethod
    def formats(cls):
        return cls.__formats.keys()


def encode_default(message, encoding, delimiter):
    if isinstance(message, Message):
        return message.as_string(encoding, delimiter)
    elif isinstance(message, bytes):
        return message
    elif isinstance(message, unicode):
        return message.encode(encoding)
    raise ValueError