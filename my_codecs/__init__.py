from StringIO import StringIO
from codecs import Codec, CodecInfo

__author__ = 'viruzzz-kun'


class koi7n1(Codec):
    def encode(self, text, errors='strict'):
        inter = text.encode('koi8-r', errors)
        output = StringIO()
        state = 0
        for char in inter:
            if ord(char) == 0xA3: char = 0xC5
            if ord(char) == 0xB3: char = 0xE5
            if 0x80 <= ord(char) < 0xC0:
                raise ValueError(u"KOI7 codec can't encode symbol")
            elif 0xC0 <= ord(char) < 0xFF:
                if state == 0:
                    state = 1
                    output.write('\x0E')
                output.write(chr(ord(char) - 0x80))
            elif 0x40 <= ord(char) < 0x7F:
                if state == 1:
                    state = 0
                    output.write('\x0F')
                output.write(char)
        return output.getvalue(), len(text)

    def decode(self, text, errors='strict'):
        output = StringIO()
        state = 0
        for char in text:
            if ord(char) == 0x0E:
                state = 1
                continue
            elif ord(char) == 0x0F:
                state = 0
                continue
            if 0x40 <= ord(char) < 0x7F:
                if state == 1:
                    output.write(chr(ord(char) + 0x80))
                else:
                    output.write(char)
        return output.getvalue().decode('koi8-r', errors), len(text)


class koi7n2(Codec):
    def encode(self, text, errors='strict'):
        inter = text.encode('koi8-r', errors)
        output = StringIO()
        for char in inter:
            if ord(char) == 0xB3: char = 0xE5
            if 0x80 <= ord(char) < 0xE0:
                raise ValueError(u"KOI7 codec can't encode symbol")
            elif 0xE0 <= ord(char) < 0xFF:
                output.write(chr(ord(char) - 0x80))
            else:
                output.write(char)
        return output.getvalue(), len(text)

    def decode(self, text, errors='strict'):
        output = StringIO()
        for char in text:
            if 0x60 <= ord(char) < 0x7F:
                output.write(chr(ord(char) + 0x80))
            else:
                output.write(char)
        return output.getvalue().decode('koi8-r', errors), len(text)


def getKoi7(name):
    if name in ('koi7n1', 'koi7-n1'):
        return CodecInfo(koi7n1().encode, koi7n1().decode)
    elif name in ('koi7n2', 'koi7-n2'):
        return CodecInfo(koi7n2().encode, koi7n2().decode)


