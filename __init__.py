# !/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright 2014 Mikhael Malkov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = 'viruzzz-kun'
__created__ = '07.06.2014'

# noinspection PyUnresolvedReferences
from .message import Message, MessageFormatRegistry, encode_default
# noinspection PyUnresolvedReferences
from .stream_protocol import MsgPackStreamProtocol as StreamProtocol