# -*- coding: utf-8 -*-

#    Copyright 2014 Mikhael Malkov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

__author__ = 'mmalkov'


def ensure_dir_exists(path):
    try:
        os.makedirs(path)
    except OSError:
        pass


def safe_traverse(obj, *args, **kwargs):
    """Safe dict traverse
    @param obj: entry point
    @param *args: keys to dig
    @param default=None: default value
    @rtype: any
    """
    default = kwargs.get('default', None)
    if obj is None:
        return default
    if len(args) == 0:
        raise ValueError(u'len(args) must be > 0')
    elif len(args) == 1:
        return obj.get(args[0], default)
    else:
        return safe_traverse(obj.get(args[0]), *args[1:], **kwargs)


def format_duration(t):
    t = int(t)
    t, seconds = divmod(t, 60)
    t, minutes = divmod(t, 60)
    t, hours = divmod(t, 24)
    return u'%d d, %02d:%02d:%02d' % (t, hours, minutes, seconds)

