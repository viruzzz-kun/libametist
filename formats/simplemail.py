# !/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright 2014 Mikhael Malkov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from email import message_from_string, email
import logging
from .. import Message, encode_default, MessageFormatRegistry

__author__ = 'viruzzz-kun'
__created__ = '15.06.2014'


def decode_email(string, encoding=None):
    """Decode email message to libametist.Message

    @param string: Message
    @type string: str
    @rtype: ametist_api.api.Message
    @raise Exception:
    """
    msg = message_from_string(string)
    content_type = msg.get_content_type()
    message = Message()
    if not content_type == 'text/plain':
        logging.critical(u'Simple mail decoder can only decode text/plain messages (%s)' % content_type)
        message.tag('raw', 'email')
        message.payload = string
    else:
        encoding = msg.get_content_charset('ascii')
        payload = msg.get_payload(decode=True)
        decode_chain = []
        try:
            payload = MessageFormatRegistry.decode('soh', payload, None).payload
            decode_chain.append('SOH')
        except ValueError:
            pass
        try:
            payload = MessageFormatRegistry.decode('zczc', payload, None).payload
            decode_chain.append('ZCZC')
        except ValueError:
            pass
        try:
            message = MessageFormatRegistry.decode('wmo', payload, encoding)
            decode_chain.append('WMO')
        except ValueError:
            try:
                message = MessageFormatRegistry.decode('hms', payload, encoding)
                decode_chain.append('HMS')
            except ValueError:
                decode_chain.append('RAW')
                message.payload = payload
                message.tag('raw')
        logging.info('Message received is %s' % ' + '.join(decode_chain))

    for key, val in msg.items():
        if key.lower().startswith('x-ametist-header-'):
            message[key:17] = val

        elif key.lower().startswith('x-ametist-tags'):
            if val.startswith('[') and val.endswith(']'):
                val = val[1:-1]
            message.tag(*val.split(','))

    return message


def encode_email(message, encoding='utf-8', delimiter='\n'):
    """Encode libametist.Message to email.Message object

    @param message: libametist.Message
    @rtype: email.Message
    @return: encoded and mostly prepared email message object"""
    if 'binary' in message.tags:
        part = email.message.Message()
        part.set_default_type('application/octet-stream')
        part.set_payload(encode_default(message, encoding, delimiter).encode('base64'), 'base64')
        msg = email.mime.multipart.MIMEMultipart(_subparts=[part])
    else:
        msg = email.message.Message()
        if 'hms' in message.tags:
            msg.set_payload(MessageFormatRegistry.encode('hms', message, encoding, delimiter), 'utf-8')
        else:
            msg.set_payload(MessageFormatRegistry.encode('wmo', message, encoding, delimiter), 'utf-8')
    msg['X-Ametist.tags'] = email.Header.Header(u', '.join(message.tags), 'utf-8')
    return msg

MessageFormatRegistry.register_format('email', encode_email, decode_email)