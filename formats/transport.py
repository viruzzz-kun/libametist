# !/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright 2014 Mikhael Malkov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .. import Message, encode_default, MessageFormatRegistry

__author__ = 'viruzzz-kun'
__created__ = '15.06.2014'


def encode_soh(message, encoding='utf-8', delimiter='\n'):
    """Encode SOH-message
    :param message: message to encode
    :param encoding: output encoding
    :param delimiter: optional string delimiter
    :type message: Message | bytes | unicode
    :rtype: bytes
    """
    return b'\01%s\03' % encode_default(message, encoding, delimiter)


def decode_soh(string, encoding=None):
    """Decodes SOH-encoded message

    @param string: SOH-encoded message
    @param encoding: suggested encoding
    @rtype: Message
    """
    if not (string.startswith('\x01') and string.endswith('\x03')):
        raise ValueError()
    result = Message()
    result.payload = string[1:-1].decode(encoding) if encoding else string[1:-1]
    result.format = 'soh'
    return result


def encode_zczc(message, encoding='utf-8', delimiter='\n'):
    """Encode ZCZC-message
    @param encoding:
    @param delimiter:
    @rtype: bytes"""
    return b'ZCZC{0}{1}{0}NNNN{0}{0}{0}{0}'.format(encode_default(message, encoding, delimiter), delimiter)


def decode_zczc(string, encoding=None):
    """Decodes ZCZC-encoded (teletype/telex) message
    @param string: ZCZC-encoded message
    @rtype: Message
    @return decoded message
    """
    zczc, nnnn = string.find(u'ZCZC'), string.rfind(u'NNNN')
    if zczc == -1 or nnnn == -1 or nnnn <= zczc:
        zczc, nnnn = string.find(u'ЗЦЗЦ'), string.rfind(u'НННН')
        if zczc == -1 or nnnn == -1 or nnnn <= zczc:
            raise ValueError(u"'string' is not ZCZC-encoded message")
    result = Message()
    payload = string[zczc + 4:nnnn].strip()
    result.payload = payload
    result.format = 'zczc'
    return result


MessageFormatRegistry.register_format('soh', encode_soh, decode_soh)
MessageFormatRegistry.register_format('zczc', encode_zczc, decode_zczc)