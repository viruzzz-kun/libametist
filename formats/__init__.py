# !/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'viruzzz-kun'
__created__ = '15.06.2014'

def __import():
    import os
    for filename in os.listdir(os.path.dirname(__file__)):
        name, ext = os.path.splitext(filename)
        if not name == '__init__':
            __import__(name, globals())

__import()